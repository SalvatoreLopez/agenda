<?php
    $respuesta = null;

    include "database.php";
    include "consultas.php";
    include "inserciones.php";
    include "actualizaciones.php";
    include "eliminaciones.php";

    $consultas = new consultas();
    $inserciones = new inserciones();
    $actualizaciones = new actualizaciones();
    $eliminaciones = new eliminaciones();

    $opcion = $_POST["opcion"];

    switch( $opcion ):

        case "agregar-nota":
            $respuesta =  $inserciones->agregarNota($_POST["title"],$_POST["start"],$_POST["end"]);
        break;

        case "notas":
            $respuesta = $consultas->notas();
        break;
        
        case "actualizar-fecha-nota":
            $respuesta = $actualizaciones->actualizarFechas($_POST["id"],$_POST["title"],$_POST["start"],$_POST["end"]);
        break;

        case "eliminar-nota":
            $respuesta = $eliminaciones->eliminarNota($_POST["id"]);
        break;

        case "actualizar-nota":
            $respuesta = $actualizaciones->actualizarNota($_POST["id"],$_POST["title"],$_POST["start"],$_POST["end"]);
        break;

    endswitch;
    echo json_encode($respuesta);
?>