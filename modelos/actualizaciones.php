<?php
    class actualizaciones{

        public function actualizarFechas(  $id , $title , $start , $end){

            $respuesta = null;

            try{
                $sql = "UPDATE notas SET title = :title , start = :start , end = :end WHERE id = :id";
                $database = new database();
                $stmt = $database->getConnection()->prepare($sql);
                $stmt->bindParam(":title",$title);
                $stmt->bindParam(":start",$start);
                $stmt->bindParam(":end",$end);
                $stmt->bindParam(":id",$id);
                $stmt->execute();
                $respuesta["estatus"] = "ok";
                $respuesta["mensaje"] = "se ha editado correctamente la fecha de la nota";
            }catch(PDOException $e){
                $respuesta["estatus"] = "error";
                $respuesta["mensaje"] = $e->getMessage();
            }

            return $respuesta;
        }

        public function actualizarNota(  $id , $title , $start , $end){

            $respuesta = null;

            try{
                $sql = "UPDATE notas SET title = :title , start = :start , end = :end WHERE id = :id";
                $database = new database();
                $stmt = $database->getConnection()->prepare($sql);
                $stmt->bindParam(":title",$title);
                $stmt->bindParam(":start",$start);
                $stmt->bindParam(":end",$end);
                $stmt->bindParam(":id",$id);
                $stmt->execute();
                $respuesta["estatus"] = "ok";
                $respuesta["mensaje"] = "se ha actualizado correctamente la nota";
            }catch(PDOException $e){
                $respuesta["estatus"] = "error";
                $respuesta["mensaje"] = $e->getMessage();
            }

            return $respuesta;
        }
    }
?>