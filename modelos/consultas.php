<?php
    class consultas{

        public function notas( ){
            $respuesta = null;
            $sql = "SELECT id ,title , start , end FROM notas ";
            try{
                $database = new database();
                $stmt = $database->getConnection()->prepare($sql);
                $stmt->execute();
                $respuesta["estatus"] = "ok";
                $respuesta["mensaje"] = $stmt->fetchAll(PDO::FETCH_ASSOC);
               
            }catch(PDOException $e){
                $respuesta["estatus"] = "error";
                $respuesta["mensaje"] = $e->getMessage();
            }

            return $respuesta;
        }

    }
?>