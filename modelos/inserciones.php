<?php
    class inserciones{
        public function agregarNota( $title , $start , $end  ){

            $respuesta = null;

            try{
                $sql = "INSERT INTO notas ( title , start , end ) VALUES ( :title , :start , :end )";
                $database = new database();
                $stmt = $database->getConnection()->prepare($sql);
                $stmt->bindParam(":title",$title);
                $stmt->bindParam(":start",$start);
                $stmt->bindParam(":end",$end);
                $stmt->execute();
                $respuesta["estatus"] = "ok";
                $respuesta["mensaje"] = "se ha creado correctamente la nota";
            }catch(PDOException $e){
                $respuesta["estatus"] = "error";
                $respuesta["mensaje"] = $e->getMessage();
            }

            return $respuesta;
        }
    }

?>