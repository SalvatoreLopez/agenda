<?php
    class eliminaciones{

        public function eliminarNota( $id ){
            $respuesta = null;
            try{
                $sql = "DELETE FROM notas WHERE id = :id ";
                $database = new database();
                $stmt = $database->getConnection()->prepare($sql);
                $stmt->bindParam(":id",$id);
                $stmt->execute();
                $respuesta['estatus'] = "ok";
                $respuesta['mensaje'] = "Se ha borrado correctamente la nota";
            }catch(PDOException $e){
                $respuesta["estatus"] = "error";
                $respuesta["mensaje"] = $e->getMessage();
            }
            return $respuesta;
        }     
    }
?>