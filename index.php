<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Agenda</title>

    <link rel="stylesheet" href="css/all.min.css">
    <link rel="stylesheet" href="css/adminlte.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body class="bg-light">
    <div class="container-fluid">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="offset-md-2 col-md-8">
                        <div class="card card-primary">
                            <div class="card-body">
                                <div id="calendar"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- opciones editar o eliminar nota -->
        <div class="modal fade" id="modal-sm">
            <div class="modal-dialog modal-sm">
                <div class="modal-content text-center">
                    <div class="modal-header">
                        <h4 class="modal-title text-center">¿Que deseás hacer?</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="text" class="form-control mb-3 start-nota d-none">
                        <input type="text" class="form-control mb-3 end-nota d-none" >
                        <input type="text" class="form-control mb-3 id-nota d-none">
                        <button class="btn btn-danger eliminar-nota">Eliminar Nota</button>
                        <button class="btn btn-primary editar-nota">Editar Nota</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- opciones editar o eliminar nota -->
        <div class="modal fade" id="modal-sm-actualizar">
            <div class="modal-dialog modal-sm">
                <div class="modal-content text-center">
                    <div class="modal-header">
                        <h4 class="modal-title text-center">Actualizar Nota</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="needs-validation" novalidate>
                            <input type="text" class="form-control title-nota" name="title" required>
                            <div class="invalid-feedback ">
                                No puede estar vacío!
                            </div>
                            <button class="btn btn-primary mt-3" >Actualizar Nota</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="js/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/es.js"></script>
    <script src="js/index.js"></script>
</body>

</html>