function cargarNotas() {
    let body = new FormData();
    const url = "modelos/index.php";
    body.append("opcion", "notas");

    fetch(url, {
        method: "POST",
        body: body
    })
        .then((resp) => resp.json())
        .then((data) => {
            if (data.estatus == "ok") {
                let notas = data.mensaje
                calendar(notas)

                console.log(data.mensaje)
            }
        })
        .catch((err) => console.error(err));
}

cargarNotas()


const calendar = (notas) => {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth'
        },
        themeSystem: 'bootstrap',
        locale: 'es',
        initialView: 'dayGridMonth',
        events: notas,
        editable: true,
        selectable: true,

        select: function (info) {
            var title = prompt("Ingresar nota");
            if (title) {

                const url = "modelos/index.php";

                let start = formatearFecha(info.startStr)
                let end = formatearFecha(info.endStr)

                let body = new FormData();

                body.append("opcion", "agregar-nota");
                body.append("title", title);
                body.append("start", start);
                body.append("end", end);

                fetch(url, {
                    method: "POST",
                    body: body
                })
                    .then((resp) => resp.json())
                    .then((data) => {
                        if (data.estatus == "ok")
                            location.reload()
                    })
                    .catch((err) => console.error(err));
            }
        },
        eventDrop: function (info) {

            let opcion = confirm("Estas seuro de hacer este cambio")
            if (opcion) {
                const url = "modelos/index.php";

                let start = formatearFecha(info.event.start)
                let end = formatearFecha(info.event.end)
                let title = info.event.title
                let id = info.event.id

                let body = new FormData();

                body.append("opcion", "actualizar-fecha-nota");
                body.append("title", title);
                body.append("start", start);
                body.append("end", end);
                body.append("id", id);

                fetch(url, {
                    method: "POST",
                    body: body
                })
                    .then((resp) => resp.json())
                    .then((data) => {
                        if (data.estatus == "ok")
                            cargarNotas()
                    })
                    .catch((err) => console.error(err));
            } else {
                info.revert();

            }
        },
        eventClick: function (info) {
            let startNota = document.querySelector(".start-nota")
            let endNota = document.querySelector(".end-nota")

            let idNota = document.querySelector(".id-nota")
            idNota.value = info.event.id

            let start = formatearFecha(info.event.start)
            let end = formatearFecha(info.event.end)

            startNota.value = start
            endNota.value = end

            let title = document.querySelector(".title-nota")
            title.value = info.event.title


            $('#modal-sm').modal({
                backdrop: 'static',
                keyboard: false
            })
        }
    });

    calendar.render();
}

const formatearFecha = (date) => moment(date).format("Y-MM-DD HH:mm:ss")


let eliminarNota = document.querySelector(".eliminar-nota");
let editarNota = document.querySelector(".editar-nota");

eliminarNota.onclick = (e) => {
    const url = "modelos/index.php";

    $('#modal-sm').modal('hide')

    let idNota = document.querySelector(".id-nota").value

    let body = new FormData();

    body.append("opcion", "eliminar-nota");
    body.append("id", idNota);

    fetch(url, {
        method: "POST",
        body: body
    })
        .then((resp) => resp.json())
        .then((data) => {
            if (data.estatus == "ok")
                cargarNotas()
        })
        .catch((err) => console.error(err));
}

editarNota.onclick = (e) => {

    const url = "modelos/index.php";

    $('#modal-sm').modal('hide')
    $('#modal-sm').on('hidden.bs.modal', function (e) {
        $('#modal-sm-actualizar').modal('show')

        var forms = document.getElementsByClassName('needs-validation');

        Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                event.preventDefault();
                event.stopPropagation();
                if (form.checkValidity()) {
                    let id = document.querySelector(".id-nota").value
                    let start = document.querySelector(".start-nota").value
                    let end = document.querySelector(".end-nota").value
                    let body = new FormData(forms[0]);

                    body.append("opcion", "actualizar-nota");
                    body.append("start", start);
                    body.append("end", end);
                    body.append("id", id);

                    fetch(url, {
                        method: "POST",
                        body: body
                    })
                        .then((resp) => resp.json())
                        .then((data) => {
                            if (data.estatus == "ok"){
                                $('#modal-sm-actualizar').modal('hide')
                                cargarNotas()
                            }
                        })
                        .catch((err) => console.error(err));
                }
                form.classList.add('was-validated');
            }, false);
        });
    })
}