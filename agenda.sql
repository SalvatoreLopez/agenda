-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-12-2020 a las 04:56:50
-- Versión del servidor: 10.4.16-MariaDB
-- Versión de PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `agenda`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas`
--

CREATE TABLE `notas` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `notas`
--

INSERT INTO `notas` (`id`, `title`, `start`, `end`) VALUES
(25, 'nota 1', '2020-12-03 00:00:00', '2020-12-04 00:00:00'),
(26, 'nota 2', '2020-12-16 00:00:00', '2020-12-17 00:00:00'),
(27, 'nota 3', '2020-12-19 00:00:00', '2020-12-20 00:00:00'),
(28, 'nota 4', '2020-12-30 00:00:00', '2020-12-31 00:00:00'),
(29, 'nota 5', '2020-12-22 00:00:00', '2020-12-23 00:00:00'),
(30, 'nota 6', '2020-12-16 00:00:00', '2020-12-17 00:00:00'),
(31, 'nota 7', '2020-12-06 00:00:00', '2020-12-07 00:00:00'),
(32, 'nota 8', '2020-12-03 00:00:00', '2020-12-04 00:00:00'),
(33, 'nota 9', '2020-12-03 00:00:00', '2020-12-04 00:00:00'),
(34, 'nota 10', '2020-12-25 00:00:00', '2020-12-26 00:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `notas`
--
ALTER TABLE `notas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `notas`
--
ALTER TABLE `notas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
